import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UUID } from 'angular2-uuid';
import {
    KfOrgHierarchyLevelEnum, KfOrgHierarchyData, KfOrgHierarchyFamily, KfOrgHierarchySubFamily, KfOrgHierarchyJob, KfOrgHierarchyBase
} from '../models/kf-org-hierarchy-data.model';

@Injectable()
export class NodeLinkDataService {
    constructor(
        private http: HttpClient,
    ) {}

    getNodeLinkReferenceData(): Observable<NodeLinkData> {
        const url = 'http://localhost:4200/assets/data/node-link-reference.json';
        return this.http.get<NodeLinkData>(url);
    }

    getKfOrgHierarchyData(level: KfOrgHierarchyLevelEnum): Observable<NodeLinkData> {
        const url = 'http://localhost:4200/assets/data/kf-org-hierarchy.json';
        return this.http.get<any>(url)
            .pipe(map((resp: any) => resp.data))
            .pipe(map((data: KfOrgHierarchyData[]) => this.kfOrgHierarchyToNodeLinkDataAdapter(data, level)));
    }

    kfOrgHierarchyToNodeLinkDataAdapter(data: KfOrgHierarchyData[], level: KfOrgHierarchyLevelEnum): NodeLinkData {
        const retVal: NodeLinkData = {
            id: UUID.UUID(),
            name: 'Korn Ferry',
            children: [],
        };

        if (level >= KfOrgHierarchyLevelEnum.FAMILY_TYPE) {
            data.forEach((datum: KfOrgHierarchyData) => {
                const familyType: NodeLinkData = {};
                familyType.id = UUID.UUID();
                familyType.name = datum.name;
                familyType.custom = this.getCustomData(datum);
                familyType.children = [];

                if (level >= KfOrgHierarchyLevelEnum.FAMILY) {
                    datum.families.forEach((family: KfOrgHierarchyFamily) => {
                        const familyHierarchy: NodeLinkData = {};
                        familyHierarchy.id = UUID.UUID();
                        familyHierarchy.name = family.name;
                        familyHierarchy.custom = this.getCustomData(family);
                        familyHierarchy.children = [];

                        if (level >= KfOrgHierarchyLevelEnum.SUB_FAMILY) {
                            family.subFamilies.forEach((subFamily: KfOrgHierarchySubFamily) => {
                                const subFamilyHierarchy: NodeLinkData = {};
                                subFamilyHierarchy.id = UUID.UUID();
                                subFamilyHierarchy.name = subFamily.name;
                                subFamilyHierarchy.custom = this.getCustomData(subFamily);
                                subFamilyHierarchy.children = [];

                                if (level >= KfOrgHierarchyLevelEnum.JOBS) {
                                    subFamily.jobs.forEach((job: KfOrgHierarchyJob) => {
                                        const jobHierarchy: NodeLinkData = {};
                                        jobHierarchy.id = UUID.UUID();
                                        jobHierarchy.name = job.name;
                                        jobHierarchy.custom = this.getCustomData(job);
                                        subFamilyHierarchy.children.push(jobHierarchy);
                                    });
                                }

                                familyHierarchy.children.push(subFamilyHierarchy);
                            });
                        }

                        familyType.children.push(familyHierarchy);
                    });
                }

                retVal.children.push(familyType);
            });
        }

        return retVal;
    }

    getCustomData(data: KfOrgHierarchyBase): KfOrgHierarchyBase {
        return {
            name: data.name,
            headCount: data.headCount,
            compRatioPercent: data.compRatioPercent,
            compRange: data.compRange,
            medianPay: data.medianPay,
            averagePay: data.averagePay,
            payrollCost: data.payrollCost,
        };
    }
}
