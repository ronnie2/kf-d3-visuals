import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CoOccurrenceMatrixData } from '../models/co-occurrence-data.model';

@Injectable()
export class CoOccurrenceMatrixDataService {
    constructor(
        private http: HttpClient,
    ) {}

    getCoOccurrenceMatrixReferenceData(): Observable<CoOccurrenceMatrixData> {
        const url = 'http://localhost:4200/assets/data/co-occurrence-matrix-reference.json';
        return this.http.get<CoOccurrenceMatrixData>(url);
    }

    getKfCoOccurrenceMatrixData(): Observable<CoOccurrenceMatrixData> {
        // TODO: Complete this once I get the KF data for this
        const url = 'http://localhost:4200/assets/data/co-occurrence-matrix-reference.json';
        return null;
    }
}
