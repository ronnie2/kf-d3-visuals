interface NodeLinkData {
    id?: string;
    name?: string;
    custom?: any;
    children?: NodeLinkData[];
}
