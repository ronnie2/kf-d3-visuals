export enum KfOrgHierarchyLevelEnum {
    FAMILY_TYPE = '1',
    FAMILY = '2',
    SUB_FAMILY = '3',
    JOBS = '4',
}

export interface KfOrgHierarchyData extends KfOrgHierarchyBase {
    families: KfOrgHierarchyFamily[];
}

export interface KfOrgHierarchyFamily extends KfOrgHierarchyBase {
    subFamilies: KfOrgHierarchySubFamily[];
}

export interface KfOrgHierarchySubFamily extends KfOrgHierarchyBase {
    jobs: KfOrgHierarchyJob[];
}

export interface KfOrgHierarchyJob extends KfOrgHierarchyBase {
    code: string;
}

export interface KfOrgHierarchyBase {
    id?: string;
    name: string;
    headCount: number;
    compRatioPercent?: number;
    compRange?: string;
    medianPay?: number;
    averagePay?: number;
    payrollCost?: number;
    hayReferenceLevel?: number;
}
