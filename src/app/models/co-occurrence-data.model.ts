export enum CoOccurrenceMatrixExampleSortOrderEnum {
    NAME = 'name',
    COUNT = 'count',
    GROUP = 'group',
}

export interface CoOccurrenceMatrixData {
    nodes: CoOccurrenceMatrixNode[];
    links: CoOccurrenceMatrixLink[];
}

export interface CoOccurrenceMatrixNode {
    name: string;
    group: number;
    count: number;
}

export interface CoOccurrenceMatrixLink {
    source: any;
    target: any;
    value: any;
}
