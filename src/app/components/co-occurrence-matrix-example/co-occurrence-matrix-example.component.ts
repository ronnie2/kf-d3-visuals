import { Component, OnInit } from '@angular/core';
import { CoOccurrenceMatrixData, CoOccurrenceMatrixExampleSortOrderEnum } from 'src/app/models/co-occurrence-data.model';
import { CoOccurrenceMatrixDataService } from 'src/app/services/co-occurrence-matrix-data.service';

enum CoOccurrenceMatrixExampleSourceEnum {
    KF_CO_OCCURRENCE_MATRIX_DATA = '1',
    CO_OCCURRENCE_MATRIX_REFERENCE_DATA = '2',
}

@Component({
    selector: 'app-co-occurrence-matrix-example',
    templateUrl: './co-occurrence-matrix-example.component.html',
    styleUrls: ['./co-occurrence-matrix-example.component.less']
})
export class CoOccurrenceMatrixExampleComponent implements OnInit {
    public data: CoOccurrenceMatrixData = null;

    public source: CoOccurrenceMatrixExampleSourceEnum = CoOccurrenceMatrixExampleSourceEnum.CO_OCCURRENCE_MATRIX_REFERENCE_DATA;

    public get Source(): CoOccurrenceMatrixExampleSourceEnum {
        return this.source;
    }

    public set Source(value: CoOccurrenceMatrixExampleSourceEnum) {
        this.source = value;
        this.refreshChart();
    }

    private sortOrder: CoOccurrenceMatrixExampleSortOrderEnum = CoOccurrenceMatrixExampleSortOrderEnum.NAME;

    public get SortOrder(): CoOccurrenceMatrixExampleSortOrderEnum {
        return this.sortOrder;
    }

    public set SortOrder(value: CoOccurrenceMatrixExampleSortOrderEnum) {
        this.sortOrder = value;
        this.refreshChart();
    }

    constructor(
        private dataService: CoOccurrenceMatrixDataService,
    ) {}

    ngOnInit() {
        this.refreshChart();
    }

    refreshChart() {
        this.data = null;
        switch (this.source) {
        case CoOccurrenceMatrixExampleSourceEnum.KF_CO_OCCURRENCE_MATRIX_DATA:
            this.dataService.getKfCoOccurrenceMatrixData().subscribe((data: CoOccurrenceMatrixData) => {
                this.data = data;
            });
            break;
        case CoOccurrenceMatrixExampleSourceEnum.CO_OCCURRENCE_MATRIX_REFERENCE_DATA:
            this.dataService.getCoOccurrenceMatrixReferenceData().subscribe((data: CoOccurrenceMatrixData) => {
                this.data = data;
            });
        }
    }
}
