import { Component, OnInit } from '@angular/core';
import { NodeLinkDataService } from 'src/app/services/node-link-data.service';
import { KfOrgHierarchyLevelEnum } from 'src/app/models/kf-org-hierarchy-data.model';

enum NodeLinkExampleSourceEnum {
    KF_ORG_HIERARCHY_DATA = '1',
    NODE_LINK_REFERENCE_DATA = '2',
}

@Component({
    selector: 'app-node-link-example',
    templateUrl: './node-link-example.component.html',
    styleUrls: ['./node-link-example.component.less']
})
export class NodeLinkExampleComponent implements OnInit {
    public source: NodeLinkExampleSourceEnum = NodeLinkExampleSourceEnum.KF_ORG_HIERARCHY_DATA;

    public get Source(): NodeLinkExampleSourceEnum {
        return this.source;
    }

    public set Source(value: NodeLinkExampleSourceEnum) {
        this.source = value;
        this.reloadData();
    }

    private level: KfOrgHierarchyLevelEnum = KfOrgHierarchyLevelEnum.SUB_FAMILY;

    public get Level(): KfOrgHierarchyLevelEnum {
        return this.level;
    }

    public set Level(value: KfOrgHierarchyLevelEnum) {
        this.level = value;
        this.reloadData();
    }

    public data: NodeLinkData = null;
    public displayData: NodeLinkData = null;
    public depthBreadCrumb = [];
    public tooltipCB = this.tooltipCBHandler.bind(this);

    constructor(
        private dataService: NodeLinkDataService,
    ) {}

    ngOnInit() {
        this.reloadData();
    }

    drilldown(event: any) {
        if (event.data.children && event.data.children.length > 0 && this.displayData !== event.data) {
            this.depthBreadCrumb = [
                ...this.depthBreadCrumb,
                ...(this.depthBreadCrumb.length > 0 ?
                        this.getDepthBreadCrumb(event).slice(1) : this.getDepthBreadCrumb(event)),
            ];
            this.refreshChart(this.getDataAtCurrentDepth());
        }
    }

    drillup(event: any) {
        if (this.depthBreadCrumb.length > 0) {
            this.depthBreadCrumb.pop();
            this.refreshChart(this.getDataAtCurrentDepth());
        }
    }

    getDepthBreadCrumb(startNode: any): any[] {
        const retVal = [];
        let node = startNode;
        while (node) {
            retVal.unshift(node.data.id);
            node = node.parent;
        }
        return retVal;
    }

    getDataAtCurrentDepth(): NodeLinkData {
        let node = this.data;
        this.depthBreadCrumb.slice(1).forEach((breadCrumb: string, index: number) => {
            node = node.children.find((childNode: NodeLinkData) => childNode.id === breadCrumb);
        });
        return node;
    }

    reloadData() {
        this.depthBreadCrumb = [];

        switch (this.source) {
        case NodeLinkExampleSourceEnum.KF_ORG_HIERARCHY_DATA:
            this.dataService.getKfOrgHierarchyData(this.level).subscribe((data: NodeLinkData) => {
                this.data = data;
                this.refreshChart(this.data);
            });
            break;
        case NodeLinkExampleSourceEnum.NODE_LINK_REFERENCE_DATA:
            this.dataService.getNodeLinkReferenceData().subscribe((data: NodeLinkData) => {
                this.data = data;
                this.refreshChart(this.data);
            });
        }
    }

    refreshChart(data: any) {
        this.displayData = null;
        setTimeout(() => {
            this.displayData = data;
        });
    }

    tooltipCBHandler(data: NodeLinkData): string {
        let retVal = '';
        switch (this.source) {
        case NodeLinkExampleSourceEnum.KF_ORG_HIERARCHY_DATA:
            retVal = this.kfHierarchyDataTooltipCBHandler(data);
            break;
        case NodeLinkExampleSourceEnum.NODE_LINK_REFERENCE_DATA:
            retVal = this.nodeLinkReferenceDataTooltipCBHandler(data);
            break;
        }
        return retVal;
    }

    kfHierarchyDataTooltipCBHandler(data: NodeLinkData): string {
        let retVal = '';

        if (data && data.custom) {
            retVal = `
                <div class="tooltip-title">Korn Ferry Hierarchy</div>
                <table>
            `;

            if (data.custom.name) {
                retVal += `
                    <tr>
                        <td class="tooltip-item-label">Name:</td>
                        <td class="tooltip-item-data">${data.custom.name}</td>
                    </tr>
                `;
            }

            if (data.custom.headCount) {
                retVal += `
                    <tr>
                        <td class="tooltip-item-label">Head Count:</td>
                        <td class="tooltip-item-data">${data.custom.headCount}</td>
                    </tr>
                `;
            }

            if (data.custom.compRatioPercent) {
                retVal += `
                    <tr>
                        <td class="tooltip-item-label">Comp Ratio Percent:</td>
                        <td class="tooltip-item-data">${data.custom.compRatioPercent}</td>
                    </tr>
                `;
            }

            if (data.custom.medianPay) {
                retVal += `
                    <tr>
                        <td class="tooltip-item-label">Median Pay:</td>
                        <td class="tooltip-item-data">${data.custom.medianPay}</td>
                    </tr>
                `;
            }

            if (data.custom.averagePay) {
                retVal += `
                    <tr>
                        <td class="tooltip-item-label">Average Pay:</td>
                        <td class="tooltip-item-data">${data.custom.averagePay}</td>
                    </tr>
                `;
            }

            if (data.custom.payrollCost) {
                retVal += `
                    <tr>
                        <td class="tooltip-item-label">Payroll Cost:</td>
                        <td class="tooltip-item-data">${data.custom.payrollCost}</td>
                    </tr>
                `;
            }

            retVal += `
                </table>
            `;
        }

        return retVal;
    }

    nodeLinkReferenceDataTooltipCBHandler(data: NodeLinkData): string {
        return `
            <div class="tooltip-title">Node Link Reference</div>
            <table>
            <tr>
                <td class="tooltip-item-label">Tooltip Field Static</td>
                <td class="tooltip-item-data">Sample Value</td>
            </tr>
            <tr>
                <td class="tooltip-item-label">Tooltip Field from Data</td>
                <td class="tooltip-item-data">${data.custom.tooltip}</td>
            </tr>
            </table>
            </div>
        `;
    }
}
