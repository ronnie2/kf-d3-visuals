import { Component, AfterContentInit, Input } from '@angular/core';
import * as d3 from 'd3';
import {
    CoOccurrenceMatrixData, CoOccurrenceMatrixLink, CoOccurrenceMatrixExampleSortOrderEnum
} from 'src/app/models/co-occurrence-data.model';

@Component({
    selector: 'app-co-occurrence-matrix',
    templateUrl: './co-occurrence-matrix.component.html',
    styleUrls: ['./co-occurrence-matrix.component.less']
})
export class CoOccurrenceMatrixComponent implements AfterContentInit {
    @Input() data: CoOccurrenceMatrixData = null;
    @Input() sortOrder: CoOccurrenceMatrixExampleSortOrderEnum = CoOccurrenceMatrixExampleSortOrderEnum.GROUP;

    svg: any = null;
    nodes: any = null;
    orders: any = null;
    margin = {
        top: 80,
        right: 0,
        bottom: 10,
        left: 80
    };
    width = 720;
    height = 720;

    x = d3.scaleBand<number>().range([0, this.width]);
    z = d3.scaleLinear().domain([0, 4]).clamp(true);
    c = d3.scaleOrdinal(d3.schemeCategory10);

    ngAfterContentInit() {
        this.svg = d3.select('#chart').append('svg')
            .attr('width', this.width + this.margin.left + this.margin.right)
            .attr('height', this.height + this.margin.top + this.margin.bottom)
            .style('margin-left', -this.margin.left + 'px')
            .append('g')
            .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');

        // d3.json('/assets/data/co-occurrence-matrix.json').then((data: CoOccurrenceMatrixData) => {
        const matrix = [];
        // const nodes = data.nodes;
        this.nodes = this.data.nodes;
        const n: number = this.nodes.length;

        // Compute index per node.
        this.nodes.forEach((node: any, i: number) => {
            node.index = i;
            node.count = 0;
            matrix[i] = d3.range(n).map((j: number) => {
                return {
                    x: j,
                    y: i,
                    z: 0
                };
            });
        });

        // Convert links to matrix; count character occurrences.
        this.data.links.forEach((link: CoOccurrenceMatrixLink) => {
            matrix[link.source][link.target].z += link.value;
            matrix[link.target][link.source].z += link.value;
            matrix[link.source][link.source].z += link.value;
            matrix[link.target][link.target].z += link.value;
            this.nodes[link.source].count += link.value;
            this.nodes[link.target].count += link.value;
        });

        // Precompute the orders.
        this.orders = {
            name: d3.range(n).sort((a: any, b: any) => {
                return d3.ascending(this.nodes[a].name, this.nodes[b].name);
            }),
            count: d3.range(n).sort((a, b) => {
                return this.nodes[b].count - this.nodes[a].count;
            }),
            group: d3.range(n).sort((a, b) => {
                return this.nodes[b].group - this.nodes[a].group;
            })
        };

        // The default sort order.
        this.x.domain(this.orders.name);

        this.svg.append('rect')
            .attr('class', 'background')
            .attr('width', this.width)
            .attr('height', this.height);

        const row = this.svg.selectAll('.row')
            .data(matrix)
            .enter().append('g')
            .attr('class', 'row')
            .attr('transform', (d: any, i: number) => {
                return 'translate(0,' + this.x(i) + ')';
            })
            .each(this.processRow.bind(this));

        row.append('line')
            .attr('x2', this.width);

        row.append('text')
            .attr('x', -6)
            .attr('y', this.x.bandwidth() / 2)
            .attr('dy', '.32em')
            .attr('text-anchor', 'end')
            .text((d, i) => {
                return this.nodes[i].name;
            });

        const column = this.svg.selectAll('.column')
            .data(matrix)
            .enter().append('g')
            .attr('class', 'column')
            .attr('transform', (d, i) => {
                return 'translate(' + this.x(i) + ')rotate(-90)';
            });

        column.append('line')
            .attr('x1', -this.width);

        column.append('text')
            .attr('x', 6)
            .attr('y', this.x.bandwidth() / 2)
            .attr('dy', '.32em')
            .attr('text-anchor', 'start')
            .text((d: any, i: number) => {
                return this.nodes[i].name;
            });

        d3.select('#order').on('change', (d: any, i: number, nodeList: any) => {
            this.order(nodeList[i].value);
        });

        this.order(this.sortOrder);
        // });
    }

    processRow(rowArg: any, i: number, nodeList: any) {
        const cell = d3.select(nodeList[i]).selectAll('.cell')
            .data(rowArg.filter((d) => {
                return d.z;
            }))
            .enter().append('rect')
            .attr('class', 'cell')
            .attr('x', (d: any) => {
                return this.x(d.x);
            })
            .attr('width', this.x.bandwidth())
            .attr('height', this.x.bandwidth())
            .style('fill-opacity', (d: any) => {
                return this.z(d.z);
            })
            .style('fill', (d: any) => {
                return this.nodes[d.x].group === this.nodes[d.y].group ? this.c('' + this.nodes[d.x].group) : null;
            })
            .on('mouseover', this.mouseover)
            .on('mouseout', this.mouseout);
    }

    order(value) {
        this.x.domain(this.orders[value]);

        const t = this.svg.transition().duration(2500);

        t.selectAll('.row')
            .delay((d: any, i: number) => {
                return this.x(i) * 4;
            })
            .attr('transform', (d: any, i: number) => {
                return 'translate(0,' + this.x(i) + ')';
            })
            .selectAll('.cell')
            .delay((d: any) => {
                return this.x(d.x) * 4;
            })
            .attr('x', (d: any) => {
                return this.x(d.x);
            });

        t.selectAll('.column')
            .delay((d: any, i: number) => {
                return this.x(i) * 4;
            })
            .attr('transform', (d: any, i: number) => {
                return 'translate(' + this.x(i) + ')rotate(-90)';
            });
    }

    mouseover(p) {
        d3.selectAll('.row text').classed('active', (d: any, i: number) => {
            return i === p.y;
        });
        d3.selectAll('.column text').classed('active', (d: any, i: number) => {
            return i === p.x;
        });
    }

    mouseout() {
        d3.selectAll('text').classed('active', false);
    }
}
