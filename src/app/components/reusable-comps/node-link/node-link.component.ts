import { Component, AfterContentInit, Input, Output, EventEmitter } from '@angular/core';
import * as d3 from 'd3';
import { tree as d3Tree, hierarchy as d3Hierarchy } from 'd3-hierarchy';

const RADIUS = 1200 / 2;

@Component({
    selector: 'app-node-link',
    templateUrl: './node-link.component.html',
    styleUrls: ['./node-link.component.less']
})
export class NodeLinkComponent implements AfterContentInit {
    radius = 10;
    treeSizeRadiusAdjustmentFactor = 120;

    @Input() data: NodeLinkData = null;
    @Input() tooltipCB: (d: NodeLinkData) => string = null;
    @Output() drilldown: EventEmitter<any> = new EventEmitter<any>();

    ngAfterContentInit() {
        const r = RADIUS;

        this.setTreeSizeRadiusAdjustmentFactor();

        const tree = d3Tree()
            // .size([360, r - 120])
            .size([360, r - this.treeSizeRadiusAdjustmentFactor])
            .separation((a, b) => {
                return (a.parent === b.parent ? 1 : 2) / a.depth;
            });

        const diagonal = d3.linkRadial()
            .angle((d: any) => d.x / 180 * Math.PI)
            .radius((d: any) => d.y - 3);

        const vis = d3.select('#chart').append('svg:svg')
            .attr('width', r * 2)
            .attr('height', r * 2)
            .append('svg:g')
            .attr('transform', 'translate(' + r + ',' + r + ')');

        const root = d3Hierarchy(this.data);
        tree(root);
        const nodes = root.descendants();
        const links = root.links();

        const link = vis.selectAll('path.link')
            .data(links)
            .enter().append('svg:path')
            .attr('class', 'link')
            .attr('d', (d: any) => diagonal(d));

        const node = vis.selectAll('g.node')
            .data(nodes)
            .enter().append('svg:g')
            .attr('class', 'node')
            .attr('transform', (d: any) => {
                return 'rotate(' + (d.x - 90) + ') translate(' + d.y + ')';
            });

        node.append('svg:circle')
            .attr('r', 4.5);

        node.append('svg:text')
            .attr('class', 'node-text')
            .attr('dx', (d: any) => {
                return d.x < 180 ? 8 : -8;
            })
            .attr('dy', '.31em')
            .attr('text-anchor', (d: any) => {
                return d.x < 180 ? 'start' : 'end';
            })
            .attr('transform', (d: any) => {
                return d.x < 180 ? null : 'rotate(180)';
            })
            .on('mouseover', (d: any, i) => {
                const content = this.tooltipCB ? this.tooltipCB(d.data) : null;
                if (content) {
                    const xpos = d3.event.pageX - 10;
                    const ypos = d3.event.pageY - 95;
                    d3.select('#tooltip').style('top', ypos + 'px').style('left', xpos + 'px').style('display', 'block');
                    d3.select('#tooltip .content').html(content);
                }
            })
            .on('mouseout', (d, i) => {
                d3.select('#tooltip').style('display', 'none');
            })
            .on('click', (d: any, i: number) => {
                this.drilldown.emit(d);
            })
            .text((d: any) => {
                return d.data.name;
            });
    }

    setTreeSizeRadiusAdjustmentFactor() {
        // const dataSize = this.getDataSize(this.data);
        // this.treeSizeRadiusAdjustmentFactor = dataSize < 1000 ? RADIUS - dataSize - 200 : 120;
        this.treeSizeRadiusAdjustmentFactor = 120;
    }

    getDataSize(node: NodeLinkData) {
        let retVal = 0;
        if (node.children) {
            retVal = node.children.length;
            node.children.forEach((childNode: NodeLinkData) => {
                retVal += this.getDataSize(childNode);
            });
        }
        return retVal;
    }
}
