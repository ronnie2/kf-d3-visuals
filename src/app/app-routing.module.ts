import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NodeLinkExampleComponent } from './components/node-link-example/node-link-example.component';
import { NyTimesExampleComponent } from './components/ny-times-example/ny-times-example.component';
import { CoOccurrenceMatrixExampleComponent } from './components/co-occurrence-matrix-example/co-occurrence-matrix-example.component';

const routes: Routes = [
    {
        path: 'node-link-example',
        component: NodeLinkExampleComponent,
    },
    {
        path: 'co-occurrence-matrix-example',
        component: CoOccurrenceMatrixExampleComponent,
    },
    {
        path: 'ny-times-example',
        component: NyTimesExampleComponent,
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
