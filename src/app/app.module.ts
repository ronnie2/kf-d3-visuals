import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatSelectModule } from '@angular/material/select';

import { AppComponent } from './app.component';
import { NodeLinkExampleComponent } from './components/node-link-example/node-link-example.component';
import { CoOccurrenceMatrixComponent } from './components/reusable-comps/co-occurrence-matrix/co-occurrence-matrix.component';
import { NyTimesExampleComponent } from './components/ny-times-example/ny-times-example.component';
import { NodeLinkDataService } from './services/node-link-data.service';
import { NodeLinkComponent } from './components/reusable-comps/node-link/node-link.component';
import { CoOccurrenceMatrixExampleComponent } from './components/co-occurrence-matrix-example/co-occurrence-matrix-example.component';
import { CoOccurrenceMatrixDataService } from './services/co-occurrence-matrix-data.service';

@NgModule({
    declarations: [
        AppComponent,
        NodeLinkComponent,
        NodeLinkExampleComponent,
        CoOccurrenceMatrixComponent,
        CoOccurrenceMatrixExampleComponent,
        NyTimesExampleComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        HttpClientModule,
        AppRoutingModule,
        MatToolbarModule,
        MatMenuModule,
        MatIconModule,
        MatButtonModule,
        MatCardModule,
        MatSelectModule,
    ],
    entryComponents: [
        NodeLinkExampleComponent,
        CoOccurrenceMatrixExampleComponent,
        NyTimesExampleComponent,
    ],
    providers: [
        NodeLinkDataService,
        CoOccurrenceMatrixDataService,
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
